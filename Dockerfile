FROM ubuntu:18.04
LABEL Maintainer="Yuri L Chuk"

ARG CONFD_VERSION=0.16.0


COPY . /home/
#https://support.hidemyass.com/hc/en-us/articles/202721456-Recommended-Linux-CLI-OpenVPNOpenVPN-Client

RUN apt update && apt install wget fping openvpn curl dialog -y


#https://vpn.hidemyass.com/vpn-config/
#login : contacterolivier
# sA9YMFsXL#42G3W



#
# Create user and group for utorrent.
#
RUN set -eux; \
    echo '--> User Setup'; \
    groupadd --gid 1001 utorrent; \
    useradd --uid 1001 --gid utorrent --groups tty --home-dir /utorrent --create-home --shell /bin/bash utorrent;

#
# Install utorrent and all required dependencies.
#
RUN echo '--> Installing packages and utserver...'; \
    apt-get update; \
    apt-get install -qy curl sudo openssl libssl1.0.0 libssl-dev vim nfs-common; \
    curl -s https://download-hr.utorrent.com/track/beta/endpoint/utserver/os/linux-i386-ubuntu-13-04/ | tar xzf - --strip-components 1 -C utorrent; \
    apt-get -y autoremove; \
    apt-get -y clean; \
    rm -rf /var/lib/apt/lists/*; \
    rm -rf /var/cache/apt/*; \
    echo '--> Setup confd'; \
    curl -fSL --output /confd https://github.com/kelseyhightower/confd/releases/download/v${CONFD_VERSION}/confd-${CONFD_VERSION}-linux-amd64; \
    chmod +x /confd; \
    echo '--> Make dirs'; \
    mkdir \
        /utorrent/shared/download \
        /utorrent/shared/torrent \
        /utorrent/shared/done \
        /utorrent/settings \
        /utorrent/temp; \
    chown -R utorrent:utorrent /utorrent;

#
# Copy confd configs and templates
#
ADD --chown=utorrent:utorrent confd/ /etc/confd/

#
# Add utorrent init script.
#
ADD --chown=utorrent:utorrent utorrent.sh /
RUN set -eux; chmod +x /utorrent.sh

WORKDIR /utorrent

#
# Start utorrent.
#
ENTRYPOINT ["/utorrent.sh"]
CMD ["/utorrent/utserver", "-settingspath", "/utorrent/settings", "-configfile", "/utorrent/shared/utserver.conf", "-logfile", "/dev/stdout"]